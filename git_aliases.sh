#!/bin/bash

## Listing aliases
git config --global alias.ls "log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --date-order --no-merges --graph"
git config --global alias.lsx "log --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --graph"
git config --global alias.ll "log --pretty=format:'%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]' --decorate --numstat"
git config --global alias.lb "log --color --date-order --graph --oneline --decorate --simplify-by-decoration --all"

## Submodule Helpers
git config --global alias.pulls "!git pull && git submodule update --init --recursive"
git config --global alias.checkouts '!f(){ git checkout "$@" && git submodule foreach --recursive git checkout "$@" && git submodule foreach --recursive git pull; }; f'
git config --global alias.pushs "!git submodule foreach --recursive git push && git push"

## Cleanup
git config --global alias.prune "remote prune origin"

## push.default
git config --global push.default simple

## color
git config --global color.ui true
