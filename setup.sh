#!/bin/bash
# rspeyer
# Setup OSX or Linux BASH

set -e
set -u

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OS=`uname`

LINUX_BASH=${DIR}/99-linux_bash
OSX_BASH=${DIR}/99-osx_bash

if [ "$OS" == "Darwin" ]
then
	if [ -z `which brew` ]
	then
		echo "Installing Homebrew"
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi

	brew install git && brew install bash-completion

	if [ ! -d "${HOME}/bashrc.d" ]
	then
		mkdir -p ${HOME}/.bashrc.d
	fi

	cp $OSX_BASH ${HOME}/.bashrc.d/
	cp ${DIR}/01-osx-git-completion ${HOME}/.bashrc.d/

	# Replace bash_profile
	cp ${HOME}/.bash_profile ${HOME}/.bash_profile.bak.$(date "+%y%d%m")
	cp ${DIR}/.bash_profile_osx ${HOME}/.bash_profile
else
	cp $LINUX_BASH ${HOME}/.bashrc.d/
fi
